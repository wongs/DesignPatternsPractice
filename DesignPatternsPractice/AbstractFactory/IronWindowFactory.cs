﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.AbstractFactory
{
    public class IronWindowFactory : IWindowFactory
    {
        public IWindow MakeWindow()
        {
            return new IronWindow();
        }

        public IWindowFittingExpert MakeFittingExpert()
        {
            return new Welder();
        }
    }
}
