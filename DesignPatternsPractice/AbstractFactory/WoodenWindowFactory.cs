﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.AbstractFactory
{
    public class WoodenWindowFactory : IWindowFactory
    {
        public IWindow MakeWindow()
        {
            return new WoodenWindow();
        }

        public IWindowFittingExpert MakeFittingExpert()
        {
            return new Carpenter();
        }
    }
}
