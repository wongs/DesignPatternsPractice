﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.AbstractFactory
{
    public class Welder : IWindowFittingExpert
    {
        public void GetDescription()
        {
            Console.WriteLine("I do iron windows");
        }
    }
}
