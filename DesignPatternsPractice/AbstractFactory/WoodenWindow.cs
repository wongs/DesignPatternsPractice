﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.AbstractFactory
{
    public class WoodenWindow : IWindow
    {
        public void GetDescription()
        {
            Console.WriteLine("Wooden Window");
        }
    }
}
