﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.FactoryMethod
{
    public abstract class HiringManager
    {
        // Factory method
        public abstract IInterviewer MakeInterviewer();
        public void TakeInterview()
        {
            var interviewer = MakeInterviewer();
            interviewer.AskQuestions();
        }
    }
}
