﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.FactoryMethod
{
    public class Teamleader : IInterviewer
    {
        public void AskQuestions()
        {
            Console.WriteLine("Ask about Team spirit.");
        }
    }
}
