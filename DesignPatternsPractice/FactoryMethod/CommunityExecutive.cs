﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.FactoryMethod
{
    public class CommunityExecutive : IInterviewer
    {
        void IInterviewer.AskQuestions()
        {
            Console.WriteLine("Ask about community building.");
        }
    }
}
