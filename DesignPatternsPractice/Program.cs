﻿using DesignPatternsPractice.AbstractFactory;
using DesignPatternsPractice.DynamicBinding;
using DesignPatternsPractice.FactoryMethod;
using DesignPatternsPractice.SimpleFactory;
using System;

namespace DesignPatternsPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Console.WriteLine();
            Console.WriteLine("Practice Simple Factory:");
            SimpleFactory();

            Console.WriteLine();
            Console.WriteLine("Practice Factory Method:");
            FactoryMethod();

            Console.WriteLine();
            Console.WriteLine("Practice Abstract Factory:");
            AbstractFactory();

            Console.WriteLine();
            Console.WriteLine("Practice Abstract Factory:");
            DynamicBindingCount();
            Console.WriteLine();
            DynamicBindingWhoAreYou();
        }

        /// <summary>
        /// Simple Factory Design Pattern.
        ///
        /// When creating an object is not just a few assignments and involves some logic,
        /// it makes sense to put it in a dedicated factory instead of repeating the same code everywhere.
        /// </summary>
        public static void SimpleFactory()
        {
            var door1 = DoorFactory.CreateGlassDoor(80, 30);
            Console.WriteLine(string.Format("Height of Door: {0}", door1.GetHeight()));
            Console.WriteLine(string.Format("Width of Door: {0}", door1.GetWidth()));

            var door2 = DoorFactory.CreateWoodenDoor(20, 50);
            Console.WriteLine(string.Format("Height of Door: {0}", door2.GetHeight()));
            Console.WriteLine(string.Format("Width of Door: {0}", door2.GetWidth()));
        }

        /// <summary>
        /// Factory Method Design Pattern.
        ///
        /// Useful when there is some generic processing in a class but the required sub-class is dynamically decided at runtime.
        /// Or putting it in other words, when the client doesn't know what exact sub-class it might need.
        /// </summary>
        public static void FactoryMethod()
        {
            var devManager = new DevelopmentManager();
            devManager.TakeInterview();

            var marketingManager = new MarketingManager();
            marketingManager.TakeInterview();
        }

        /// <summary>
        /// Abstract Factory Design Pattern
        ///
        /// When there are interrelated dependencies with not-that-simple creation logic involved.
        /// </summary>
        public static void AbstractFactory()
        {
            var woodenWindowFactory = new WoodenWindowFactory();

            var woodenWindow = woodenWindowFactory.MakeWindow();
            var woodenWindowFittingExpert = woodenWindowFactory.MakeFittingExpert();

            woodenWindow.GetDescription(); //Output : I am a wooden door
            woodenWindowFittingExpert.GetDescription();//Output : I can only fit woooden doors

            var ironWindowFactory = new IronWindowFactory();

            var ironWindow = ironWindowFactory.MakeWindow();
            var ironWindowFittingExpert = ironWindowFactory.MakeFittingExpert();

            ironWindow.GetDescription();//Output : I am a iron door
            ironWindowFittingExpert.GetDescription();//Output : I can only fit iron doors
        }

        /// <summary>
        /// Dynamic Binding
        ///
        ///
        /// </summary>
        public static void DynamicBindingCount()
        {
            A objectA = new A();
            B objectB = new B();

            objectA.IncreaseCount();
            objectB.IncreaseCount();

            Console.WriteLine(objectA.Count);
            Console.WriteLine(objectB.Count);

            A object3 = new B();
            object3.IncreaseCount();

            Console.WriteLine(object3.Count);

            A object4 = new C();
            object4.IncreaseCount();
            Console.WriteLine(object4);

        }

        public static void DynamicBindingWhoAreYou()
        {
            A object1 = new A();
            B object2 = new B();

            var object3 = new D();
            A object4 = new C();

            B object5 = new D();
            D object6 = new D();
            A object7 = new D();

            object3.WhoAreYou();
            object4.WhoAreYou();
            object5.WhoAreYou();
            object6.WhoAreYou();
            object7.WhoAreYou();
        }
    }
}
