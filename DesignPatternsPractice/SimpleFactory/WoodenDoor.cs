﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternsPractice.SimpleFactory
{
    public class WoodenDoor : IDoor
    {
        private int _height;
        private int _width;

        public WoodenDoor(int height, int width)
        {
            _height = height;
            _width = width;
        }

        public int GetHeight()
        {
            return _height;
        }

        public int GetWidth()
        {
            return _width;
        }

    }
}
