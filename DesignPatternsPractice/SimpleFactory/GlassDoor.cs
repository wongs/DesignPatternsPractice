﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternsPractice.SimpleFactory
{
    public class GlassDoor : IDoor
    {
        private int _height;
        private int _width;
        private int _countGlasses;
        private double _sizeGlasses;

        public GlassDoor(int height, int width)
        {
            _height = height;
            _width = width;
        }

        public int GetHeight()
        {
            return _height;
        }

        public int GetWidth()
        {
            return _width;
        }

        public int GetCountGlasses()
        {
            return _countGlasses;
        }

        public double GetSizeGlasses()
        {
            return _sizeGlasses;
        }
    }
}
