﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternsPractice.SimpleFactory
{
    public static class DoorFactory
    {

        public static IDoor CreateWoodenDoor(int height, int width)
        {
            return new WoodenDoor(height, width);
        }

        public static IDoor CreateGlassDoor(int height, int width)
        {
            return new GlassDoor(height, width);
        }
    }
}
