﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternsPractice.SimpleFactory
{
    public interface IDoor
    {
        int GetHeight();
        int GetWidth();
    }
}
