﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsPractice.DynamicBinding
{
    public class A
    {
        public int Count { get; protected set; }
        public virtual void WhoAreYou()
        {
            Console.WriteLine("I'm A");
        }

        public virtual void IncreaseCount()
        {
            Count++;
        }
    }

    public class B : A
    {
        public override void IncreaseCount()
        {
            Count += 5;
        }

        public override void WhoAreYou()
        {
            Console.WriteLine("I'm B");
        }
    }

    public class C : B
    {
        public override void WhoAreYou()
        {
            Console.WriteLine("I'm C");
        }
    }

    public class D : C
    {
        public override void WhoAreYou()
        {
            Console.WriteLine("I'm D");
        }
    }

}
