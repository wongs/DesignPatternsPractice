Project to practice Design Patterns

Practiced Patterns:

- Simple Factory
- Factory Method
- Abstract Factory
- Dynamic Binding
